# encoding: utf-8
# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name        = "acbapis-sdk"
  s.license     = "Nonstandard"
  s.version     = "0.0.1"
  s.authors     = ["Joan Llopis"]
  s.email       = ["jllopis@acb.es"]
  s.homepage    = "https://bitbucket.org/acbapis/acbapis-ruby-sdk"
  s.summary     = %q{"ACB APIs gRPC SDK for Ruby"}
  s.description = %q{"Implements access to ACB APIs using the gRPC version in lang Ruby."}
  s.files = Dir['lib/**/*.rb']
  s.require_paths = ["lib"]
  s.required_ruby_version = ">= 1.9"
  s.rdoc_options = ["--charset=UTF-8"]

  s.add_runtime_dependency 'grpc', '~> 1.7', '>= 1.7.2'
end
