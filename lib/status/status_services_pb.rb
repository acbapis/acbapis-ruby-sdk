# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: status/status.proto for package 'status'

require 'grpc'
require 'status/status_pb'

module Status
  module StatusService
    class Service

      include GRPC::GenericService

      self.marshal_class_method = :encode
      self.unmarshal_class_method = :decode
      self.service_name = 'status.StatusService'

      rpc :GetServerTime, Common::EmptyMessage, ServerTimeMessage
      rpc :GetVersion, Common::EmptyMessage, Common::Version
      rpc :GetGlobalServiceStatus, Common::EmptyMessage, ServerStatusMessage
    end

    Stub = Service.rpc_stub_class
  end
end
