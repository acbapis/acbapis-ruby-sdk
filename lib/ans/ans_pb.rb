# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ans/ans.proto

require 'google/protobuf'

require 'google/api/annotations_pb'
require 'common/common_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("ans/ans.proto", :syntax => :proto3) do
    add_message "ans.Account" do
      optional :id, :string, 1
      optional :nombre, :string, 2
      optional :email, :string, 3
      optional :direccion, :string, 4
      optional :poblacion, :string, 5
      optional :provincia, :string, 6
      optional :cp, :string, 7
      optional :pais, :string, 8
      optional :telefono, :string, 9
      optional :movil, :string, 10
      optional :empresa, :string, 11
      optional :web, :string, 12
      optional :activo, :bool, 13
      optional :created, :int64, 14
      optional :updated, :int64, 15
      optional :deleted, :int64, 16
    end
    add_message "ans.Service" do
      optional :id, :string, 1
      optional :label, :string, 2
      optional :description, :string, 3
      optional :active, :bool, 4
      optional :created, :int64, 5
      optional :updated, :int64, 6
      optional :deleted, :int64, 7
      repeated :filters, :string, 8
      optional :topic, :string, 9
    end
    add_message "ans.ServiceArray" do
      repeated :services, :message, 1, "ans.Service"
      optional :total, :int32, 2
    end
    add_message "ans.ServiceSorting" do
      optional :id, :string, 1
      optional :label, :string, 2
      optional :description, :string, 3
      optional :active, :string, 4
      optional :created, :string, 5
      optional :updated, :string, 6
      optional :deleted, :string, 7
    end
    add_message "ans.ServiceFilter" do
      optional :offset, :int64, 1
      optional :limit, :int64, 2
      optional :terms, :string, 3
      optional :active, :int32, 4
      optional :sorting, :message, 5, "ans.ServiceSorting"
    end
    add_message "ans.Filter" do
      optional :label, :string, 1
      optional :code, :string, 2
    end
    add_message "ans.Credentials" do
      optional :id, :string, 1
      optional :type, :enum, 2, "ans.Credentials.Type"
      optional :username, :string, 3
      optional :password, :string, 4
      optional :token, :string, 5
      optional :cert, :bytes, 6
      optional :ftpHost, :string, 7
      optional :ftpPort, :int32, 8
    end
    add_enum "ans.Credentials.Type" do
      value :UNKNOWN, 0
      value :BASIC, 1
      value :TOKEN, 2
      value :CERT, 3
    end
    add_message "ans.Subscription" do
      optional :id, :string, 1
      optional :accountId, :string, 2
      optional :account, :message, 3, "ans.Account"
      optional :serviceId, :string, 4
      optional :service, :message, 5, "ans.Service"
      optional :formatId, :string, 6
      optional :format, :message, 7, "ans.Format"
      optional :endpoint, :string, 8
      optional :kind, :enum, 9, "ans.SubscriptionKind"
      optional :authRequired, :bool, 10
      optional :credentials, :message, 11, "ans.Credentials"
      repeated :preFilter, :message, 12, "ans.Filter"
      optional :ftpDestination, :string, 13
      repeated :emailTo, :string, 14
      optional :emailSubject, :string, 15
      optional :active, :bool, 16
      optional :created, :int64, 17
      optional :updated, :int64, 18
      optional :deleted, :int64, 19
    end
    add_message "ans.SubscriptionArray" do
      repeated :subscriptions, :message, 1, "ans.Subscription"
      optional :total, :int32, 2
    end
    add_message "ans.SubscriptionSorting" do
      optional :id, :string, 1
      optional :accountId, :string, 2
      optional :serviceId, :string, 3
      optional :formatId, :string, 4
      optional :endpoint, :string, 5
      optional :kind, :string, 6
      optional :authRequired, :string, 7
      optional :active, :string, 8
      optional :created, :string, 9
      optional :updated, :string, 10
      optional :deleted, :string, 11
    end
    add_message "ans.SubscriptionFilter" do
      optional :offset, :int64, 1
      optional :limit, :int64, 2
      optional :accountId, :string, 3
      optional :serviceId, :string, 4
      optional :formatId, :string, 5
      optional :kind, :string, 6
      optional :authRequired, :int32, 7
      optional :active, :int32, 8
      optional :sorting, :message, 9, "ans.SubscriptionSorting"
      optional :serviceLabel, :string, 10
    end
    add_message "ans.Format" do
      optional :id, :string, 1
      optional :serviceId, :string, 2
      optional :accountId, :string, 3
      optional :formatType, :string, 4
      optional :tmpl, :bytes, 5
      optional :tmplVersion, :string, 6
      optional :active, :bool, 7
      optional :created, :int64, 8
      optional :updated, :int64, 9
      optional :deleted, :int64, 10
    end
    add_message "ans.FormatArray" do
      repeated :formats, :message, 1, "ans.Format"
      optional :total, :int32, 2
    end
    add_message "ans.FormatSorting" do
      optional :id, :string, 1
      optional :serviceId, :string, 2
      optional :accountId, :string, 3
      optional :formatType, :string, 4
      optional :tmpl, :string, 5
      optional :tmplVersion, :string, 6
      optional :active, :string, 7
      optional :created, :string, 8
      optional :updated, :string, 9
      optional :deleted, :string, 10
    end
    add_message "ans.FormatFilter" do
      optional :offset, :int64, 1
      optional :limit, :int64, 2
      optional :serviceId, :string, 3
      optional :accountId, :string, 4
      optional :formatType, :string, 5
      optional :tmplVersion, :string, 6
      optional :active, :int32, 7
      optional :sorting, :message, 8, "ans.FormatSorting"
    end
    add_message "ans.ErrorNotification" do
      optional :service, :string, 1
      optional :message, :string, 2
    end
    add_enum "ans.SubscriptionKind" do
      value :UNKNOWN, 0
      value :MAIL, 1
      value :FTP, 2
      value :HTTP, 3
    end
  end
end

module Ans
  Account = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Account").msgclass
  Service = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Service").msgclass
  ServiceArray = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.ServiceArray").msgclass
  ServiceSorting = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.ServiceSorting").msgclass
  ServiceFilter = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.ServiceFilter").msgclass
  Filter = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Filter").msgclass
  Credentials = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Credentials").msgclass
  Credentials::Type = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Credentials.Type").enummodule
  Subscription = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Subscription").msgclass
  SubscriptionArray = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.SubscriptionArray").msgclass
  SubscriptionSorting = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.SubscriptionSorting").msgclass
  SubscriptionFilter = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.SubscriptionFilter").msgclass
  Format = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.Format").msgclass
  FormatArray = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.FormatArray").msgclass
  FormatSorting = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.FormatSorting").msgclass
  FormatFilter = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.FormatFilter").msgclass
  ErrorNotification = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.ErrorNotification").msgclass
  SubscriptionKind = Google::Protobuf::DescriptorPool.generated_pool.lookup("ans.SubscriptionKind").enummodule
end
