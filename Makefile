.DEFAULT_GOAL := all
GEM?= $(shell which gem)

all: build git

build:
	@$(GEM) build acbapis_sdk.gemspec

git:
	git add . && git commit -am "Updated lib" && git push
